/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import org.eclipse.osbp.bpm.api.IBlipBPMOutgoing;

public class BlipBPMOutgoing implements IBlipBPMOutgoing {

	protected final String blipId;
	protected final String bpmId;
	protected final int priority;
	protected final boolean isDefault;
	protected final String testFunction;

	public BlipBPMOutgoing(String blipId, String bpmId, int priority) {
		this.blipId = blipId;
		this.bpmId = bpmId;
		this.priority = priority;
		this.isDefault = true;
		this.testFunction = BlipBaseFunctionGroup.DEFAULT_SEQUENCE_FLOW;
	}

	public BlipBPMOutgoing(String blipId, String bpmId, int priority, String testFunction) {
		this.blipId = blipId;
		this.bpmId = bpmId;
		this.priority = priority;
		this.isDefault = false;
		this.testFunction = testFunction;
	}
	
	@Override
	public String getBlipId() {
		return blipId;
	}
	
	@Override
	public String getBpmId() {
		return bpmId;
	}

	@Override
	public int getPriority() {
		return priority;
	}

	@Override
	public boolean isDefault() {
		return isDefault;
	}

	@Override
	public String getTestFunction() {
		return testFunction;
	}
}