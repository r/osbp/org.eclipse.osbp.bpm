/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import org.eclipse.osbp.bpm.api.IBlipBPMConstants;
import org.eclipse.osbp.bpm.api.IBlipBPMProcessProtocol;
import org.eclipse.osbp.bpm.api.IBlipBPMProcessProtocolCache;
import org.eclipse.osbp.dsl.common.datatypes.IDto;

/**
 *  Base class for blip function groups. It provides basic functions to handle blips.
 */
public abstract class BlipBaseFunctionGroup {	// NOSONAR

	private static IBlipBPMProcessProtocolCache protocolCache;

	public static final void setProtocolCache(IBlipBPMProcessProtocolCache protocolCache) {
		if	(BlipBaseFunctionGroup.protocolCache == null || BlipBaseFunctionGroup.protocolCache != protocolCache) {
			BlipBaseFunctionGroup.protocolCache = protocolCache;
		}
	}
	
	/**
	 *  This method can be used as the default sequence flow on exclusive split gateways!
	 *  @param  kcontext  should be the {@link org.drools.runtime.process.ProcessContext} available in BPM (Java/mvel) scripts
	 *  @return <b>always</b> <code>true</code>
	 */
	public static final boolean defaultSequenceFlow(Object kcontext) {	// NOSONAR
		return true;
	}

	/**
	 *  name of the function defaultSequenceFlow(kcontext) to be used in the blip grammar
	 */
	protected static final String DEFAULT_SEQUENCE_FLOW = "defaultSequenceFlow";

	/**
	 *  @see    hints in {@link #getVariable(Object, String)
	 *  @param  kcontext  should be the {@link org.drools.runtime.process.ProcessContext} available in BPM (Java/mvel) scripts
	 *  @return the protocol of this process instance, which can be generated with appendProcessProtocol()
	 */
	public static final IBlipBPMProcessProtocol getProcessProtocol(Object kcontext) {
		return protocolCache.mergeToProtocol(kcontext, (IBlipBPMProcessProtocol)getVariable(kcontext, IBlipBPMConstants.VARIABLE_PROCESS_PROTOCOL));
	}
	
	/**
	 *  @see    hints in {@link #getVariable(Object, String)
	 *  @param  kcontext  should be the {@link org.drools.runtime.process.ProcessContext} available in BPM (Java/mvel) scripts
	 *  @param  line      new line for the protocol
	 *  @return the new protocol of this process instance
	 */
	public static final IBlipBPMProcessProtocol appendProcessProtocol(Object kcontext, String line) {
		IBlipBPMProcessProtocol protocol = protocolCache.addToProtocol(kcontext, line, getProcessProtocol(kcontext));
		setVariable(kcontext, IBlipBPMConstants.VARIABLE_PROCESS_PROTOCOL, protocol);
		return protocol;
	}
	
	/**
	 *  @see    hints in {@link #getVariable(Object, String)
	 *  @param  kcontext  should be the {@link org.drools.runtime.process.ProcessContext} available in BPM (Java/mvel) scripts
	 *  @return the workload dto of this process instance
	 */
	public static final Object getWorkloadDto(Object kcontext) {
		return getVariable(kcontext, IBlipBPMConstants.VARIABLE_PROCESS_WORKLOAD_DTO);
	}

	/**
	 *  @see    hints in {@link #getVariable(Object, String)
	 *  @param  kcontext    should be the {@link org.drools.runtime.process.ProcessContext} available in BPM (Java/mvel) scripts
	 *  @param  workloadDto the new/modified workload dto for this process instance
	 */
	public static final void setWorkloadDto(Object kcontext, IDto workloadDto) {
		setVariable(kcontext, IBlipBPMConstants.VARIABLE_PROCESS_WORKLOAD_DTO, workloadDto);
	}

	/**
	 *  @see    hints in {@link #getVariable(Object, String)
	 *  @param  kcontext  should be the {@link org.drools.runtime.process.ProcessContext} available in BPM (Java/mvel) scripts
	 *  @return the full qualified name for the workload dto used in this process instance
	 */
	public static final Object getWorkloadDtoFQN(Object kcontext) {
		return getVariable(kcontext, IBlipBPMConstants.VARIABLE_PROCESS_WORKLOAD_DTO_FQN);
	}
	
	/**
	 *  Local process variables are available in
	 *  <ul>
	 *  <li>in <i>User Tasks</i> ony, if they are mapped from local process variables to task variables</li>
	 *  <li>in <i>Script Tasks</i>, <i>Gateways</i>, etc. via (Java/mvel) scripts</li>
	 *  </ul> 
	 *  @param  kcontext  should be the {@link org.drools.runtime.process.ProcessContext} available in BPM (Java/mvel) scripts
	 *  @param  variable  name of the process instance variable
	 *  @return the value of the process instance variable or <code>null</code>
	 */
	protected static final Object getVariable(Object kcontext, String variable) {
		return getVariable(kcontext, variable, null);
	}
	
	/**
	 *  Local process variables are available in
	 *  <ul>
	 *  <li>in <i>User Tasks</i> ony, if they are mapped from local process variables to task variables</li>
	 *  <li>in <i>Script Tasks</i>, <i>Gateways</i>, etc. via (Java/mvel) scripts</li>
	 *  </ul> 
	 *  @param  kcontext       should be the {@link org.drools.runtime.process.ProcessContext} available in BPM (Java/mvel) scripts
	 *  @param  variable       name of the process instance variable
	 *  @param  defaultValue   default value, if the variable has not been set before
	 *  @return the value of the process instance variable or <code>defaultValue</code>
	 */
	protected static final Object getVariable(Object kcontext, String variable, Object defaultValue) {
		if(ServiceBinder.getBPMEngine() != null) {
			return ServiceBinder.getBPMEngine().getVariable(kcontext, variable, defaultValue);
		}
		return null;
	}

	/**
	 *  @see    hints in {@link #getVariable(Object, String)
	 *  @param  kcontext  should be the {@link org.drools.runtime.process.ProcessContext} available in BPM (Java/mvel) scripts
	 *  @param  variable  name of the process instance variable to be set
	 *  @param  value     the new value of the process instance variable
	 */
	protected static final void setVariable(Object kcontext, String variable, Object value) {
		if(ServiceBinder.getBPMEngine() != null) {
			ServiceBinder.getBPMEngine().setVariable(kcontext, variable, value);
		}
	}
}
