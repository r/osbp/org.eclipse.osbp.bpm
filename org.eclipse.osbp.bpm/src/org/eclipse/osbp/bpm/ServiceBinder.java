/*
 *                                                                            
 *  Copyright (c) 2011 - 2017 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                              
 * 
 */
package org.eclipse.osbp.bpm;

import org.eclipse.osbp.bpm.api.IBPMEngine;
import org.eclipse.osbp.bpm.api.IBPMTaskClient;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class ServiceBinder {
	private static IBPMEngine bpmEngine;
	private static IBPMTaskClient taskClient;
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger("servicebinder");

	/**
	 * Bind bpm engine.
	 *
	 * @param bpmEngine
	 */
	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
	public synchronized void bindBPMEngine(final IBPMEngine bpmEngine) {
		ServiceBinder.setBPMEngine(bpmEngine);
		LOGGER.debug("BPMEngine bound");
	}

	/**
	 * Unbind bpm engine.
	 *
	 * @param bpmEngine
	 */
	public synchronized void unbindBPMEngine(final IBPMEngine bpmEngine) { // NOSONAR
		ServiceBinder.setBPMEngine(null);
		LOGGER.debug("BPMEngine unbound");
	}

	public static IBPMEngine getBPMEngine() {
		return ServiceBinder.bpmEngine;
	}

	public static void setBPMEngine(IBPMEngine bpmEngine) {
		ServiceBinder.bpmEngine = bpmEngine;
	}

	/**
	 * Bind task client.
	 *
	 * @param taskClient the task client
	 */
	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
	public synchronized void bindTaskClient(final IBPMTaskClient taskClient) {
		ServiceBinder.setTaskClient(taskClient);
		LOGGER.debug("TaskClient bound");
	}

	/**
	 * Unbind task client.
	 *
	 * @param taskClient the task client
	 */
	public synchronized void unbindTaskClient(final IBPMTaskClient taskClient) { // NOSONAR
		ServiceBinder.setTaskClient(null);
		LOGGER.debug("TaskClient unbound");
	}

	public static IBPMTaskClient getTaskClient() {
		return ServiceBinder.taskClient;
	}

	public static void setTaskClient(IBPMTaskClient taskClient) {
		ServiceBinder.taskClient = taskClient;
	}
}
