/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import org.eclipse.osbp.bpm.api.IBPMServiceTask;
import org.eclipse.osbp.bpm.api.ServiceExecutionMode;
import org.eclipse.osbp.bpm.api.ServiceImplementation;

/**
 * API for a bpm system task
 */
public abstract class AbstractBPMServiceTask extends AbstractBlipBPMItem implements IBPMServiceTask {

	protected final String onEntryFunction;
	protected final String onExitFunction;
	protected final ServiceExecutionMode executionMode;
	protected final ServiceImplementation implementation;
	protected final int timeoutInSecs;
	protected final String classOrInterface;
	protected final String methodOrOperation;
	
	/** see {@link org.eclipse.osbp.bpm.api.AbstractBlipBPMItem#AbstractBPMItem(String, String)} */
	protected AbstractBPMServiceTask(String blipId, String bpmId, String onEntryFunction, String onExitFunction,
			ServiceExecutionMode executionMode, int timeoutInSecs,
			ServiceImplementation implementation, String classOrInterface, String methodOrOperation) {
		super(blipId, bpmId);
		this.onEntryFunction = onEntryFunction;
		this.onExitFunction = onExitFunction;
		this.executionMode = executionMode;
		this.timeoutInSecs = (timeoutInSecs > 0) ? timeoutInSecs : 10;
		this.implementation = implementation;
		this.classOrInterface = classOrInterface;
		this.methodOrOperation = methodOrOperation; 
	}

	@Override
	public final String getOnEntryFunction() {
		return onEntryFunction;
	}

	@Override
	public final String getOnExitFunction() {
		return onExitFunction;
	}
	
	@Override
	public ServiceExecutionMode getExecutionMode() {
		return executionMode;
	}

	@Override
	public int getTimeoutInSecs() {
		return timeoutInSecs;
	}

	@Override
	public ServiceImplementation getImplementation() {
		return implementation;
	}

	@Override
	public String getFunctionLibraryClass() {
		if	(implementation.equals(ServiceImplementation.FUNCTION_LIBRARY)) {
			return classOrInterface;
		}
		return null;
	}

	@Override
	public String getFunctionLibraryMethod() {
		if	(implementation.equals(ServiceImplementation.FUNCTION_LIBRARY)) {
			return methodOrOperation;
		}
		return null;
	}

	@Override
	public String getWebServiceInterface() {
		if	(implementation.equals(ServiceImplementation.WEB_SERVICE)) {
			return classOrInterface;
		}
		return null;
	}

	@Override
	public String getWebServiceOperation() {
		if	(implementation.equals(ServiceImplementation.WEB_SERVICE)) {
			return methodOrOperation;
		}
		return null;
	}

	@Override
	public String getJavaInterface() {
		if	(implementation.equals(ServiceImplementation.JAVA_METHOD)) {
			return classOrInterface;
		}
		return null;
	}

	@Override
	public String getJavaOperation() {
		if	(implementation.equals(ServiceImplementation.JAVA_METHOD)) {
			return methodOrOperation;
		}
		return null;
	}

	@Override
	public String getParameterType() {
		Class<?> clazz = getOperativeDtoClass();
		if	(clazz == null) {
			return null;
		}
		else {
			return clazz.getCanonicalName();
		}
	}
}
