/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import java.util.List;

import org.eclipse.osbp.bpm.api.IBlipBPMUserTask;
import org.eclipse.osbp.dsl.common.datatypes.IDto;


/**
 * API for a bpm sub process
 */
public class BPMCallActivity extends AbstractBlipBPMItem implements IBlipBPMUserTask {

	protected final String onEntryFunction;
	protected final String onExitFunction;
	
	/** see {@link org.eclipse.osbp.bpm.api.AbstractBlipBPMItem#AbstractBPMItem(String, String)} */
	public BPMCallActivity(String blipId, String bpmId, String onEntryFunction, String onExitFunction) {
		super(blipId, bpmId);
		this.onEntryFunction = onEntryFunction;
		this.onExitFunction = onExitFunction;
	}

	@Override
	public final String getOnEntryFunction() {
		return onEntryFunction;
	}

	@Override
	public final String getOnExitFunction() {
		return onExitFunction;
	}

	@Override
	public Class<?> getOperativeDtoClass() {
		return null;
	}

	@Override
	public List<IDto> getOperativeDtos(IDto workloadDto) {
		return null;
	}
}
