/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

public class NamingConvention {

	private NamingConvention() {}
	
	public static String getBpmItemRecommendedName(String name) {
		String recommended = name;
		recommended = recommended.replace('_', ' ');
		recommended = recommended.replaceAll("  ", " ");
		recommended = StringUtils.stripAccents(recommended);
		recommended = WordUtils.capitalize(recommended);
		recommended = recommended.replace(" ", "");
		return recommended;
	}
}
