/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.osbp.bpm.api.BPMTaskSummary;
import org.eclipse.osbp.bpm.api.IBlipBPMFunctionProvider;
import org.eclipse.osbp.bpm.api.IBlipBPMStartInfo;

@SuppressWarnings("all")
public abstract class AbstractBlipBPMFunctionProvider implements IBlipBPMFunctionProvider {
	protected Map<String, IBlipBPMStartInfo> startInfos;

	public AbstractBlipBPMFunctionProvider() {
		super();
		startInfos = new HashMap<String, IBlipBPMStartInfo>();
	}

	@Override
	public IBlipBPMStartInfo getStartInfo(BPMTaskSummary taskSummary) {
		IBlipBPMStartInfo startInfo = null;
		String processId = getProcessId(taskSummary);
		if	(processId != null) {
			startInfo = startInfos.get(processId);
		}
		return startInfo;
	}
	
	protected String getProcessId(BPMTaskSummary taskSummary) {
		if	(ServiceBinder.getTaskClient() != null) {
			return ServiceBinder.getTaskClient().getProcessId(taskSummary);
		}
		return null;
	}
	
	@Override
	public final Map<String, IBlipBPMStartInfo> getBpmns() {
		return startInfos;
	}
}
