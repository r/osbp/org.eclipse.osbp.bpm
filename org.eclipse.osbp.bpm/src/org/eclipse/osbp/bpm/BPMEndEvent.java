/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

/**
 * API for a bpm end event.
 */
public final class BPMEndEvent extends AbstractBPMEvent {

	protected final boolean terminatesProcess;
	
	/** see {@link org.eclipse.osbp.bpm.api.AbstractBPMEvent#AbstractBPMEvent(String, String)} */
	public BPMEndEvent(String blipId, String bpmId, boolean terminatesProcess) {
		super(blipId, bpmId);
		this.terminatesProcess = terminatesProcess;
	}

	public boolean isTerminatesProcess() {
		return terminatesProcess;
	}
}
